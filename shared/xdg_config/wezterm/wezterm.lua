local wezterm = require 'wezterm'
local is_unix = package.config:sub(1, 1) == '/';


local function tbl_merge(t, source)
    for key, value in pairs(source) do
        t[key] = value;
    end

    return t;
end


local config = {
    audible_bell                 = 'Disabled',
    canonicalize_pasted_newlines = "CarriageReturnAndLineFeed",
    check_for_updates            = false,
    enable_scroll_bar            = false,
    hide_tab_bar_if_only_one_tab = true,

    -- hinting and aliasing options
    freetype_load_target         = 'Light',
}

if is_unix then
    config = tbl_merge(config, {
        window_decorations = 'None',
        font               = wezterm.font_with_fallback {
            { family = 'ZedMono Nerd Font', weight = 'Bold' },
            'Monospace'
        }
    });
end

config.harfbuzz_features = { 'zero' }

config.font_size         = 10.5
config.line_height       = 1.3
config.cell_width        = 1.1

config.color_scheme      = 'nord'

return config;
