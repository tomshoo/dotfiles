local CACHE_P = vim.fn.stdpath('cache');

local function setup_treesitter_queries()
    local out_p   = CACHE_P .. '/blade';
    local query_p = CACHE_P .. '/after'

    local repo    = 'https://github.com/EmranMR/tree-sitter-blade'

    if 0 == vim.fn.isdirectory(out_p) then
        vim.fn.system({ 'git', 'clone', repo, out_p });
    end

    if 0 == vim.fn.isdirectory(query_p) then
        vim.fn.mkdir(query_p .. '/queries/blade', 'p');
    end

    for _, ts_query_p in ipairs(vim.split(vim.fn.glob(out_p .. '/queries/*.scm'), '\n')) do
        local path       = vim.split(ts_query_p, '/');
        local query_path = path[#path];

        vim.uv.fs_copyfile(ts_query_p, query_p .. '/queries/blade/' .. query_path);
    end

    vim.opt.rtp:append(query_p)
end

local function setup_laravel_devtools()
    local out_p = CACHE_P .. '/laravel-dev-tools';
    local repo = 'https://github.com/haringsrob/laravel-dev-tools.git'

    if 0 == vim.fn.isdirectory(out_p) then
        vim.fn.system({ 'git', 'clone', repo, '--depth', '1', out_p })
    end
end

local function main()
    setup_treesitter_queries()
    setup_laravel_devtools()
end

main()
