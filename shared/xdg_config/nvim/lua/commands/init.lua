local scratch = require 'commands.scratch'

vim.api.nvim_create_user_command('Bclose', function(opts)
    if vim.api.nvim_get_current_buf() == scratch.scratch_buffer then
        vim.cmd.Scratch()
        return
    end

    vim.bo.bufhidden = 'delete'
    vim.cmd.bprevious { bang = opts.bang }
end, {})

vim.api.nvim_create_user_command('Scratch', scratch.call, {
    bang = true,
})

vim.api.nvim_create_user_command('SplitTerminal', function()
    local winheight = 15

    if os.getenv('TMUX') ~= nil then
        vim.fn.system({ 'tmux', 'split-window', '-v', '-l', ('%d'):format(winheight) });
        return
    end

    vim.api.nvim_open_win(0, true, {
        split  = 'below',
        height = winheight,
    });

    vim.cmd.terminal()
    vim.cmd.normal 'i'
end, {})
