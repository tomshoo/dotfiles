local M = {
    scratch_buffer = nil
}

local function get_scratch_win()
    for _, win in ipairs(vim.api.nvim_tabpage_list_wins(0)) do
        local buffer = vim.api.nvim_win_get_buf(win)

        if buffer == M.scratch_buffer then
            return win
        end
    end

    return nil
end

M.call = function(opts)
    if vim.api.nvim_get_current_buf() == M.scratch_buffer then
        vim.api.nvim_win_hide(vim.api.nvim_get_current_win())
        return
    end

    if M.scratch_buffer == nil then
        M.scratch_buffer = vim.api.nvim_create_buf(false, true)
    end

    if opts.bang == true then
        vim.api.nvim_set_current_buf(M.scratch_buffer)
        return
    end

    local scratch_win = get_scratch_win()

    if scratch_win ~= nil then
        vim.api.nvim_set_current_win(scratch_win)
        return
    end

    vim.api.nvim_open_win(M.scratch_buffer, true, {
        split = 'below',
        height = 20,
    })
end

return M
