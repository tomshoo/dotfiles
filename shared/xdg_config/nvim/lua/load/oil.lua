return {
    'stevearc/oil.nvim',
    opts = {
        default_file_explorer = true,
        watch_for_changes     = true,
        columns               = {
            "icon",
            "permissions",
        }
    },
    lazy = false,
    keys = {
        { [[<leader>f]], "<cmd>vsplit|Oil<cr>" }
    },
}
