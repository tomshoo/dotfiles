return {
    { 'kyazdani42/nvim-web-devicons', lazy = false },

    { 'kylechui/nvim-surround',       opts = {} },
    { 'kevinhwang91/nvim-hlslens',    opts = {} },
    { 'lewis6991/gitsigns.nvim',      opts = {},   event = 'BufEnter' },

    'nvim-lua/plenary.nvim',
    'direnv/direnv.vim',
    'antoinemadec/FixCursorHold.nvim',
    'christoomey/vim-tmux-navigator',

    {
        'godlygeek/tabular',
        cmd = { 'Tabularize' },
    },

    {
        'mbbill/undotree',
        keys = {
            { [[\\]], vim.cmd.UndotreeToggle, desc = 'Open undo hostory' }
        },
    },
}
