local cmp = require 'cmp'

return {
    sources = cmp.config.sources {
        { name = 'snippy' },
        { name = 'lazydev', group_index = 0 },
        { name = 'nvim_lsp' },
        { name = 'nvim_lua' },
    }
}
