return {
    {
        'windwp/nvim-autopairs',
        event  = 'InsertEnter',
        opts   = {
            disable_filetypes = { "Telescope", "vim" },
        },

        config = function(_, opts)
            local npairs = require 'nvim-autopairs'
            local rule   = require 'nvim-autopairs.rule'

            npairs.setup(opts);

            npairs.add_rules({
                rule('/**', ' */', { '-lua', '-vim', '-python' }),
                rule('{{--', '--', { 'blade' }),
                rule('{!!', '!!', { 'blade' }),
            })
        end
    },

    {
        'windwp/nvim-ts-autotag',
        lazy = false,
        opts = {
            opts = {
                enable_close          = true,
                enable_rename         = true,
                enable_close_on_slash = false
            },
        },

        config = function(_, opts)
            require('nvim-ts-autotag').setup(opts)
        end
    }
}
