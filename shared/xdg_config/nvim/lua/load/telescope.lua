local M = {}

M.setup = function()
    local actions   = require 'telescope.actions'
    local telescope = require 'telescope'

    telescope.setup {
        pickers    = {
            lsp_definitions = { theme = 'cursor' },
            lsp_references  = { theme = 'cursor' },

            buffers         = {
                theme = 'ivy',
                previewer = true,
                mappings = {
                    n = {
                        ['<C-d>'] = actions.delete_buffer + actions.move_to_top,
                    }
                },
            },

            find_files      = {
                theme     = 'ivy',
                previewer = true,
            },

            live_grep       = {
                theme     = 'ivy',
                previewer = true,
            },
        },
        extensions = {
            ['ui-select'] = { require('telescope.themes').get_cursor() },
        },
    }

    telescope.load_extension 'ui-select'
end

return {
    'nvim-telescope/telescope.nvim',

    dependencies = {
        'nvim-telescope/telescope-ui-select.nvim',
    },

    keys = {
        { '<leader><leader>', '<cmd>Telescope find_files<cr>', desc = 'Find files in current directory' },
        { '<leader>bb',       '<cmd>Telescope buffers<cr>',    desc = 'Find in active buffers' },
        { '<leader>/',        '<cmd>Telescope live_grep<cr>',  desc = 'Find files in current directory' },
        { '<F1>',             '<cmd>Telescope help_tags<cr>',  desc = 'Find files in current directory' },

        -- Lsp bindings
        { 'gd', '<cmd>Telescope lsp_definitions<cr>', desc = 'Go to definitions' },
        { 'gr', '<cmd>Telescope lsp_references<cr>',  desc = 'Go to references' },
    },

    config = M.setup,
}
