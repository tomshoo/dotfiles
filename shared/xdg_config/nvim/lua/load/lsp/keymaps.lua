return function(bufnr)
    map('n', [[\a]], vim.lsp.buf.code_action, { buffer = bufnr, desc = "Show code actions" })
    map('n', [[\r]], vim.lsp.buf.rename, { buffer = bufnr, desc = "Rename symbol undor cursor" })

    map('n', [[\cx]], vim.diagnostic.setqflist,
        { desc = "Show diagnostic errors (In project workspace)", buffer = bufnr })

    map('n', [[\cb]], vim.diagnostic.setloclist,
        { desc = "Show diagnostic errors (current buffer only)", buffer = bufnr })

    map('i', '<C-k>', vim.lsp.buf.signature_help, { desc = "Show signature help", buffer = bufnr })
end
