local function setup()
    local null = require 'null-ls'
    local fmt  = null.builtins.formatting
    local diag = null.builtins.diagnostics

    null.setup { sources = {
        fmt.black,
        fmt.prettier,
        fmt.pint,
        fmt.blade_formatter.with { filetypes = { 'blade' } },
        diag.mypy,
    } }
end

return {
    'nvimtools/none-ls.nvim',
    config = setup
}
