local function setup()
    require('mason').setup {}
    require('mason-lspconfig').setup {}

    local lspconfig = require 'lspconfig'
    local servers   = require 'load.lsp.servers'

    for server, config in pairs(servers) do
        config.capabilities = require('cmp_nvim_lsp').default_capabilities(
            vim.lsp.protocol.make_client_capabilities()
        )

        config.capabilities.textDocument.foldinRange = {
            dynamicRegistration = false,
            lineFoldingOnly     = true,
        }

        lspconfig[server].setup(config)
    end
end

return {
    require 'load.lsp.null-ls',
    require 'load.lsp.rust',
    {
        "adalessa/laravel.nvim",
        dependencies = {
            "tpope/vim-dotenv",
            "nvim-telescope/telescope.nvim",
            "MunifTanjim/nui.nvim",
            "kevinhwang91/promise-async",
        },
        cmd = { "Laravel" },
        keys = {
            { "\\la",  "<cmd>Laravel artisan<cr>" },
            { "\\lr",  "<cmd>Laravel routes<cr>" },
            { "\\lm",  "<cmd>Laravel related<cr>" },
            { "\\lvf", "<cmd>Laravel view_finder<cr>" },
        },
        config = true,
    },
    {
        'neovim/nvim-lspconfig',
        dependencies = {
            'williamboman/mason.nvim',
            'williamboman/mason-lspconfig.nvim',
            {
                "folke/lazydev.nvim",
                ft = "lua",
                opts = {
                    library = {
                        { path = "${3rd}/luv/library", words = { "vim%.uv" } },
                    },
                },
            },
        },
        config = setup,
    }
}
