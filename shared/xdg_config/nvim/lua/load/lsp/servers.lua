local configs   = require 'lspconfig.configs'
local lspconfig = require 'lspconfig'
local servers   = {}

local function php_root_dir(filename)
    local root = lspconfig.util.root_pattern('composer.json', '.phpactor.json', '.phpactor.yaml')(filename);
    local cwd  = vim.loop.cwd();

    return lspconfig.util.find_git_ancestor(filename)
        or lspconfig.util.path.is_descendant(cwd, root) and cwd
        or root
end


configs.blade = {
    default_config = {
        cmd       = { "laravel-dev-tools", "lsp" },
        filetypes = { 'blade' },
        root_dir  = php_root_dir,
        settings  = {},
    },
}


servers.prismals             = {}
servers.volar                = {}
servers.bashls               = { filetypes = { "sh", "bash" } }
servers.nil_ls               = {}
servers.perlnavigator        = {}
servers.gopls                = {}
servers.jedi_language_server = {}
servers.blade                = {}
servers.solargraph           = {}
servers.taplo                = {}

servers.lua_ls               = {
    settings = {
        Lua = {
            telimitery = { enable = false },
            workspace  = {
                library = vim.api.nvim_get_runtime_file("", true),
            },
        },
    },
}

servers.clangd               = {
    on_attach = function()
        local clangd_ext = require('clangd_extensions.inlay_hints')
        clangd_ext.setup_autocmd()
        clangd_ext.set_inlay_hints()
    end
}

servers.intelephense         = {
    filetypes = { "php", "blade" },
    root_dir  = php_root_dir,
}

-- servers.phpactor             = {
--     root_dir = php_root_dir
-- }

servers.zls                  = {
    settings = {
        enable_autofix = false,
        warn_style     = true,
    },
    on_attach = function()
        vim.g.zig_fmt_parse_errors = false
    end
}

servers.ts_ls                = {
    init_options = {
        plugins = {
            {
                name = "@vue/typescript-plugin",
                location = '/usr/lib/node_modules/@vue/typescript-plugin',
                languages = { "javascript", "typescript", "vue" },
            },
        },
    },
    filetypes = {
        "typescript",
        "javascript",
        "vue"
    }
}


return servers
