local server_settings = {
    standalone        = true,
    ['rust-analyzer'] = {
        cachePriming = true,
        check        = {
            command    = 'clippy',
            allTargets = false,
        },
        diagnostics  = { disabled = { 'inactive-code' } }
    },
}

local function setup()
    vim.api.nvim_create_autocmd({ "LspAttach" }, {
        group    = vim.api.nvim_create_augroup('rust-analyzer', { clear = true }),
        pattern  = { '*.rs' },
        callback = function(args)
            map('n', '<leader>e', '<cmd>RustLsp expandMacro<cr>', { desc = "Expand macros", buffer = args.buf })
        end
    })

    vim.g.rustaceanvim = {
        server = { default_settings = server_settings }
    }
end

return {
    'mrcjkb/rustaceanvim',
    version = '^4',
    lazy    = false,
    config  = setup,
}
