return {
    "tpope/vim-fugitive",
    lazy = false,
    keys = {
        { "<leader>gg", vim.cmd.Git, { desc = "Become fugitive" } },
    },
}
