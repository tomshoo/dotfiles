local treesitter_text_objects = {
    ['select'] = {
        enable    = true,
        lookahead = true,

        keymaps   = {
            ["af"] = "@function.outer",
            ["if"] = "@function.inner",
            ["ac"] = "@class.outer",
            ["ic"] = "@class.inner",
            ["ib"] = "@block.inner",
            ["ab"] = "@block.outer"
        }
    }
}

local function setup(_, opts)
    local treesitter    = require 'nvim-treesitter.configs'
    local ts_context    = require 'treesitter-context'
    local parser_config = require "nvim-treesitter.parsers".get_parser_configs()

    parser_config.blade = {
        install_info = {
            url    = "https://github.com/EmranMR/tree-sitter-blade",
            files  = { "src/parser.c" },
            branch = "main",
        },
        filetype = "blade"
    }

    ts_context.setup {
        max_lines = 3,
        on_attach = function(bufnr)
            vim.keymap.set('n', '[c', ts_context.go_to_context, { silent = true, desc = "Go to context", buffer = bufnr })
            return true
        end
    }

    treesitter.setup(opts)
end

return {
    'nvim-treesitter/nvim-treesitter',
    config       = setup,
    build        = function()
        require("nvim-treesitter.install").update({ with_sync = true })()
    end,
    opts         = {
        sync_install = false,
        textobjects  = treesitter_text_objects,
        highlight    = {
            enable = true,
            disable = function(_lang, buf)
                local max_size  = 100 * 1024;
                local ok, stats = pcall(vim.loop.fs_fstat, vim.api.nvim_buf_get_name(buf))

                if ok and stats then
                    return stats.size < max_size;
                else
                    return false;
                end
            end
        },
    },
    dependencies = {
        'nvim-treesitter/nvim-treesitter-context',
        'nvim-treesitter/nvim-treesitter-textobjects',
    },
}
