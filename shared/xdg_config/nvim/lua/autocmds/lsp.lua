local function load_formatting(client, bufnr)
    vim.api.nvim_create_autocmd({ "BufWritePre" }, {
        buffer   = bufnr,
        group    = vim.api.nvim_create_augroup('autoformat', { clear = true }),
        callback = function()
            vim.lsp.buf.format({
                async = vim.fn.has('g:async_formatting') == 1 or false,
                bufnr = bufnr,
                id    = client.id
            })
        end
    })
end

vim.api.nvim_create_autocmd({ "LspAttach" }, {
    group    = vim.api.nvim_create_augroup('lspattach', { clear = true }),
    callback = function(args)
        local bufnr  = args.buf
        local client = vim.lsp.get_client_by_id(args.data.client_id)

        if client == nil then
            return
        end

        require('load.lsp.highlight')(client, bufnr)
        require('load.lsp.keymaps')(bufnr)

        if client.supports_method('textDocument/formatting', { bufnr = bufnr }) then
            load_formatting(client, bufnr)
        end
    end
})
