require 'autocmds.lsp'

vim.api.nvim_create_autocmd({ 'BufWritePre' }, {
    group    = vim.api.nvim_create_augroup('Mkdir', { clear = true }),
    callback = function()
        local dir = vim.fn.expand('<afile>:p:h')

        if vim.fn.isdirectory(dir) == 0 then
            vim.fn.mkdir(dir, 'p')
        end
    end
})

vim.api.nvim_create_autocmd({ 'TextYankPost' }, {
    group    = vim.api.nvim_create_augroup('YankHighlight', { clear = true }),
    callback = function()
        vim.highlight.on_yank()
    end
})

vim.api.nvim_create_autocmd({ 'BufEnter', 'BufNew' }, {
    group = vim.api.nvim_create_augroup('BladeFt', { clear = true }),
    pattern = { '*.blade.php' },
    callback = function()
        -- Load html and php snippets

        local ls_ok, luasnip = pcall(require, 'luasnip')
        if ls_ok then luasnip.filetype_extend('blade', { 'html', 'php' }) end
    end
})

vim.api.nvim_create_autocmd({ 'BufEnter' }, {
    group    = vim.api.nvim_create_augroup('DotenvDiagnostics', { clear = true }),
    pattern  = { '.env', '.env.*' },
    callback = function(args)
        vim.diagnostic.enable(false, { bufnr = args.buf })
    end
})

vim.api.nvim_create_autocmd({ 'TermOpen' }, {
    group    = vim.api.nvim_create_augroup('NvimTermConfiguration', { clear = true }),
    callback = function()
        vim.opt.number = false
        vim.opt.relativenumber = false
    end
})

vim.api.nvim_create_autocmd({ 'BufNewFile' }, {
    group = vim.api.nvim_create_augroup('LoadTemplate', { clear = true }),
    callback = function()
        local filetype = vim.bo.filetype
        local skelpath = vim.fn.stdpath('config') .. '/skel/' .. filetype .. '.skel'

        if vim.fn.filereadable(skelpath) ~= 1 then
            return
        end

        vim.cmd.read(skelpath)
        vim.api.nvim_feedkeys('ggdd', 'n', false)

        if vim.fn.search('@placeholder@', 'n') ~= 0 then
            vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('/@placeholder@<cr>', true, false, true), 'n', false)
        else
            vim.api.nvim_feedkeys('G', 'n', false)
        end
    end
})
