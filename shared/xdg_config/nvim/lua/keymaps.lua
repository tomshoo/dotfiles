function _G.map(mode, key, action, opts)
    local default_opts = {
        remap  = true,
        silent = true
    }

    opts = vim.tbl_extend('force', opts or {}, default_opts)
    vim.keymap.set(mode, key, action, opts)
end

vim.keymap.set("n", "<Space>", "<Nop>")
vim.g.mapleader = " "

map('n', '<esc>', vim.cmd.nohlsearch, { remap = true })

map('n', 'U', vim.cmd.redo, { remap = true })

map('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
map('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

map('n', '<leader>p', '"+p')
map('v', '<leader>p', '"_dp')

map('v', '>', '>gv^', { remap = false })
map('v', '<', '<gv^', { remap = false })

map('n', '<leader>bk', vim.cmd.Bclose)

map('n', '<leader>`', vim.cmd.SplitTerminal, { desc = "Spawn a new terminal" })
map('n', '<leader>x', vim.cmd.Scratch, { desc = "Opens a scratch buffer" })

map('n', [[\cf]], '<cmd>call setqflist([], "r")<cr>', { desc = 'Clear quickfix list' })
