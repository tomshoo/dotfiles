source <(starship init zsh --print-full-init)

function zle_line_init() {
    ## source: https://github.com/romkatv/powerlevel10k/issues/888#issuecomment-657969840
    emulate -L zsh

    [[ $CONTEXT == start ]] || return 0

    while true; do
        zle .recursive-edit
        local -i ret=$?
        [[ $ret == 0 && $KEYS == $'\4' ]] || break
        [[ -o ignore_eof ]] || exit 0
    done

    local saved_prompt=$PROMPT
    local saved_rprompt=$RPROMPT

    PROMPT=$(starship  module -s ${STARSHIP_CMD_STATUS:-0} character)
    RPROMPT=$(starship module -s ${STARSHIP_CMD_STATUS:-0} status)

    zle .reset-prompt

    PROMPT=$saved_prompt
    RPROMPT=$saved_rprompt

    if (( ret )); then
        zle .send-break
    else
        zle .accept-line
    fi

    return ret
}

zle -N zle_line_init
