[[ -d "$XDG_CONFIG_HOME/composer/vendor/bin" ]] && path+=("$XDG_CONFIG_HOME/composer/vendor/bin")
[[ -d "$XDG_CONFIG_HOME/emacs/bin" ]]           && path+=("$XDG_CONFIG_HOME/emacs/bin")
[[ -d "$HOME/.config/composer/vendor/bin" ]]    && path+=("$HOME/.config/composer/vendor/bin")

export EDITOR=${EDITOR:-nvim}

if [[ -d ~/.zsh.d ]]; then
    while read file; do
        [[ -s $file ]] && source $file
    done < <(find ~/.zsh.d -maxdepth 1 -mindepth 1 -name '*.zsh' | sort)
fi

! [[ -o interactive ]] && return

function __load_file()   { [ -r "$1" ] && source "$1"; }

zstyle ':completion:*' menu select
zstyle ':completion:*:options' description 'yes'
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

__load_file "$ZDOTDIR/zsh_pre_compinit"
__load_file "$ZDOTDIR/opam.zsh"

autoload -Uz compinit     && compinit -d ~/.cache/zcompdump
autoload -Uz bashcompinit && bashcompinit

(( $+commands[zoxide] ))   && source <(zoxide init zsh --cmd cd)
(( $+commands[direnv] ))   && source <(direnv hook zsh)
(( $+commands[starship] )) && __load_file "$ZDOTDIR/starship.zsh"

(( $+functions[zsh_greeting] )) && zsh_greeting

__load_file /usr/share/fzf/key-bindings.zsh || __load_file /usr/share/doc/fzf/examples/key-bindings.zsh || zsh_add_snippet 'fzf:shell/key-bindings.zsh'
# source <(fzf --zsh)

set-window-title() {
  window_title="\e]0;${${PWD/#"$HOME"/~}/projects/p}\a"
  echo -ne "$window_title"
}; set-window-title
add-zsh-hook precmd set-window-title

bindkey ' '  _expand_abbrevation
bindkey '^ ' magic-space
bindkey -M isearch ' ' magic-space
