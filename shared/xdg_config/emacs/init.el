(defun disable-line-numbers-mode ()
    (display-line-numbers-mode 0))

(setq inhibit-startup-message 1
      show-trailing-whitespace t
      visible-bell t
      display-line-numbers-type 'relative)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(global-display-line-numbers-mode 1)
(column-number-mode t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)

(menu-bar-mode -1)

(load-theme 'wombat)

(set-face-attribute 'default nil :font "ZedMono NFM")

(dolist
    (mode '(term-mode-hook eshell-mode-hook shell-mode-hook undo-tree-visualizer-mode))
    (add-hook mode #'disable-line-numbers-mode))

(add-hook 'emacs-lisp-mode-hook
          (lambda () (setq-local lisp-indent-function #'common-lisp-indent-function)))

;; Package Manager =============================================================
;;
(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/"))

(package-initialize)

(unless package-archive-contents
    (package-refresh-contents))

(unless (package-installed-p 'use-package)
    (package-isntall 'use-package))

(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; Diminish ====================================================================
;;
(use-package diminish)

;; General =====================================================================
;;
(use-package general
    :init
    (general-evil-setup)

    :config
    (general-create-definer my/evil-keydom
        :prefix "<leader>"))

;; Better undo =================================================================
;;
(use-package undo-tree
    :diminish

    :init
    (global-undo-tree-mode))

;; I am evil ===================================================================
;;
(use-package evil
    :init
    (setq evil-want-keybinding nil
          evil-want-C-u-scroll t
          evil-want-integration t
          evil-undo-system 'undo-tree)

    :config
    (evil-mode 1)
    (evil-set-leader nil (kbd "SPC")))

(use-package evil-collection
    :diminish evil-collection-unimpaired-mode

    :after evil

    :config
    (evil-collection-init))

(use-package evil-surround
    :config
    (global-evil-surround-mode 1))

;; Better IDO UI ===============================================================
;;
(use-package vertico
    :init
    (vertico-mode)

    :bind
    (:map vertico-map
          ("C-j" . vertico-next)
          ("C-k" . vertico-previous)))

;; Lisp is readable now ========================================================
;;
(use-package rainbow-delimiters
    :hook
    (prog-mode . rainbow-delimiters-mode))

;; I cannot remember my keybinds ===============================================
;;
(if (< emacs-major-version 30)
    (use-package which-key
        :init
        (which-key-mode)

        :diminish
        which-key-mode

        :config
        (setq which-key-idle-delay 0.3)))

;; Where are my Git repos? =====================================================
;;
(use-package projectile
    :diminish

    :init
    (projectile-mode +1)

    :general
    (my/evil-keydom :states 'normal "p" 'projectile-command-map))

;; Live previews ===============================================================
;;
(use-package consult
    :diminish

    :general
    (my/evil-keydom :states 'normal "b b" 'consult-buffer)
    (my/evil-keydom :states 'normal "/" 'consult-ripgrep))

;; I don't know git... =========================================================
;;
(use-package magit)

;; Language specific settings ==================================================
;;
(use-package lsp-ui
    :config
    (general-define-key
     :states 'normal :keymaps 'lsp-ui-mode-map "K" 'lsp-ui-doc-glance))

(use-package lsp-mode
    :commands
    (lsp lsp-deferred)

    :config
    (lsp-enable-which-key-integration t)

    :general
    (my/evil-keydom :states 'normal "l" lsp-command-map))

;; Completions =================================================================
;;
(use-package company
    :after lsp-mode
    :diminish)

;; PHP =========================================================================
;;
(use-package php-mode
    :hook (php-mode . lsp-deferred)

    :custom
    (php-mode-coding-style 'psr2))

;; Web Mode ====================================================================
;;
(use-package web-mode)

;; Rust Mode ===================================================================
;;
(use-package rust-mode
    :hook (rust-mode . eglot-ensure))

(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs
               '((rust-mode) . ("rust-analyzer"
				:initializationOptions (:check (:command "clippy"))))))

;; Auto-generated ==============================================================
;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(evil-surround rust-mode web-mode lsp-ui consult company php-mode lsp-mode evil-magit magit general projectile undo-tree compat ## diminish)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
