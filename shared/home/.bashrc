set bell-style none

export fpath=(~/.config/bash/functions)

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

autoload () {
    for path in "${fpath[@]}"; do
        ! [ -d "$path" ] && continue;
        readarray -td ':' sources < <(command find "$path" -mindepth 1 -maxdepth 2 -type f -printf '%p:')

        while IFS= read -r -u3 -d $'\0' file; do
            function_name="$(basename "$file")"
            eval "
            $function_name () {
                unset -f $function_name
                source $file; $function_name \"\$@\"
            }
            ";
        done 3< <(find "$path" -mindepth 1 -maxdepth 2 -type f -print0)
    done
}

## Load custom plugins if any
if [ -d ~/.config/bash/plugins ]; then
    while IFS= read -r -u3 -d $'\0' file; do
        source "$file"
    done 3< <(find ~/.config/bash/plugins -mindepth 1 -maxdepth 1 -type f -print0)
fi

if [ -d ~/.bash.d ]; then
    while IFS= read -r -u3 -d $'\0' file; do
        source "$file"
    done 3< <(find ~/.bash.d -mindepth 1 -maxdepth 2 -type f -print0)
fi

## Autoload custom functions
autoload

# User specific environment
prepend_path "$HOME/.local/bin"
prepend_path "$HOME/.config/emacs/bin"

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -la'
alias grep='grep --color=auto'

if _cmd_exists eza; then
    alias ls='eza'
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
[ -s "$HOME/.cargo/env" ]         && \. "$HOME/.cargo/env"
